package com.moneycontrol.bot.chatbot.config;

import com.ibm.watson.developer_cloud.discovery.v1.Discovery;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author by vivekkothari on 16/12/17.
 */
@Configuration
public class DiscoveryConfig {

    @Bean
    public Discovery discovery(@Value("${watson.discovery.password}") String discoveryPassword,
                               @Value("${watson.discovery.username}") String discoveryUsername) {
        Discovery discovery = new Discovery("2017-11-07");
        discovery.setEndPoint("https://gateway.watsonplatform.net/discovery/api/");
        discovery.setUsernameAndPassword(discoveryUsername, discoveryPassword);
        return discovery;
    }


}
