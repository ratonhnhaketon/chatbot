package com.moneycontrol.bot.chatbot.impl;

import com.ibm.watson.developer_cloud.conversation.v1.Conversation;
import com.ibm.watson.developer_cloud.conversation.v1.model.InputData;
import com.ibm.watson.developer_cloud.conversation.v1.model.MessageOptions;
import com.ibm.watson.developer_cloud.conversation.v1.model.MessageResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by ANTRIKSH on 16-12-2017.
 */
@Component
public class ConversationClient {

    @Value("${watson.conversation.workspace.id}")
    private String workspaceId;

    @Value("${watson.conversation.username}")
    private String username;

    @Value("${watson.conversation.password}")
    private String password;

    public MessageResponse query(String userQuery) {
        Conversation service = new Conversation(Conversation.VERSION_DATE_2017_05_26);
        service.setUsernameAndPassword(username, password);

        InputData input = new InputData.Builder(userQuery).build();
        MessageOptions options = new MessageOptions.Builder(workspaceId).input(input).build();

        return service.message(options).execute();
    }
}
