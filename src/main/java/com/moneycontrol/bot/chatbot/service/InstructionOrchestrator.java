package com.moneycontrol.bot.chatbot.service;

import com.ibm.watson.developer_cloud.conversation.v1.model.MessageResponse;
import com.moneycontrol.bot.chatbot.api.QueryResponse;
import com.moneycontrol.bot.chatbot.impl.ConversationClient;
import com.moneycontrol.bot.chatbot.impl.DiscoveryClient;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class InstructionOrchestrator {

    private final DiscoveryClient discoveryClient;
    private final ConversationClient conversationClient;

    private static final Logger log = LoggerFactory.getLogger(InstructionOrchestrator.class);

    @Autowired
    public InstructionOrchestrator(
            DiscoveryClient discoveryClient,
            ConversationClient conversationClient
    ) {
        this.discoveryClient = discoveryClient;
        this.conversationClient = conversationClient;
    }

    QueryResponse execute(String query) {
        MessageResponse response = conversationClient.query(query);
        if (response.getOutput().containsKey("text")
                && response.getOutput().get("text").toString().contains("call_discovery")) {
            String originalQuery = response.getInput().getText();
            if (!StringUtils.isEmpty(originalQuery)) {
                try {
                    return discoveryClient.discover(originalQuery);
                } catch (Exception e) {
                    e.printStackTrace();
                    //TODO- Handle Below Exception Gracefully
                    throw new IllegalArgumentException("DISCOVERY SERVIC CALL DIDN'T GO THROUGH");
                }
            } else {
                //TODO- Handle Below Exception Gracefully
                throw new IllegalArgumentException("EMPTY_MESSAGE");
            }
        } else {
            return new QueryResponse(response, Optional.empty(), Optional.empty());
        }
    }
}
