package com.moneycontrol.bot.chatbot.service;

import com.ibm.watson.developer_cloud.conversation.v1.model.MessageResponse;
import com.moneycontrol.bot.chatbot.api.QResponse;
import com.moneycontrol.bot.chatbot.api.QueryResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

/**
 * @author by vivekkothari on 16/12/17.
 */
@Path("/v1/bot")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@Component
public class BotController {

    private final InstructionOrchestrator instructionOrchestrator;

    @Autowired
    public BotController(InstructionOrchestrator InstructionOrchestrator) {
        this.instructionOrchestrator = InstructionOrchestrator;
    }

    @GET
    public String hello() {
        return "hello";
    }

    @GET
    @Path("/search")
    public QResponse search(@QueryParam("q") final String query) throws Exception {
        QueryResponse response = instructionOrchestrator.execute(query);
        Object conversationResponse = response.getConversationResponse();
        if (conversationResponse.getClass().isAssignableFrom(MessageResponse.class)) {
            MessageResponse conResponse = (MessageResponse) conversationResponse;
            return buildConversationReponse(conResponse);
        } else if (conversationResponse.getClass().isAssignableFrom(LinkedHashMap.class)) {
            return buildDiscoveryResponse((LinkedHashMap<String, Object>) conversationResponse);
        } else {
            return new QResponse("I am not sure about that.", Collections.emptyList(), "");
        }
    }

    private QResponse buildConversationReponse(MessageResponse messageResponse) {
        return new QResponse(String.join(", ", messageResponse.getOutput().getText()), Collections.emptyList(), "");
    }

    private QResponse buildDiscoveryResponse(LinkedHashMap<String, Object> discoveryResponse) {
        String spokenPassage = "I am not sure about that.";
        List<String> relevantPassages = Collections.emptyList();
        String url = "";
        List<Map<String, String>> passages = (List<Map<String, String>>) (discoveryResponse).get("passages");
        if (passages != null && !passages.isEmpty()) {
            spokenPassage = passages.get(0).get("passage_text");
            passages.remove(0);
            relevantPassages = passages.stream().map(p -> p.get("passage_text")).collect(Collectors.toList());
        }
        List<Map<String, Map<String, String>>> results = (List<Map<String, Map<String, String>>>) discoveryResponse.get("results");
        if (!results.isEmpty()) {
            String fileName = results.get(0).get("extracted_metadata").get("filename");
            url = "http://" + fileName.replaceAll("~", "/");
        }
        return new QResponse(spokenPassage, relevantPassages, url);
    }


}
