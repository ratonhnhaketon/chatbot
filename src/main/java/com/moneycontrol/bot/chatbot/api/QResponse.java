package com.moneycontrol.bot.chatbot.api;

import java.util.List;

/**
 * @author by vivekkothari on 17/12/17.
 */
public class QResponse {

    private String spokenPassage;
    private List<String> relevantPassages;
    private String url;

    public QResponse() {

    }

    public QResponse(String spokenPassage, List<String> relevantPassages, String url) {
        this.spokenPassage = spokenPassage;
        this.relevantPassages = relevantPassages;
        this.url = url;
    }

    public String getSpokenPassage() {
        return spokenPassage;
    }

    public void setSpokenPassage(String spokenPassage) {
        this.spokenPassage = spokenPassage;
    }

    public List<String> getRelevantPassages() {
        return relevantPassages;
    }

    public void setRelevantPassages(List<String> relevantPassages) {
        this.relevantPassages = relevantPassages;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
