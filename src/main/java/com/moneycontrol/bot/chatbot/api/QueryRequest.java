package com.moneycontrol.bot.chatbot.api;

/**
 * @author by vivekkothari on 16/12/17.
 */
public class QueryRequest {

    private String queryText;

    public void setQueryText(String queryText) {
        this.queryText = queryText;
    }

    public String getQueryText() {
        return queryText;
    }
}
