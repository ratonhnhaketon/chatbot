nlp = window.nlp_compromise;

var messages = [], //array that hold the record of each string in chat

    lastUserMessage = "", //keeps track of the most recent input string from the user

    botMessage = "", //var keeps track of what the chatbot is going to say

    spokenMessage = "",

    botName = 'BudgetBot', //name of the chatbot

    talking = true; //when false the speech function doesn't work

//edit this function to change what the chatbot says
function chatbotResponse(postCallback) {
    talking = true;
    botMessage = "I'm confused"; //the default message
    $.getJSON("http://localhost:8081/chatbot/v1/bot/search?q=" + lastUserMessage)
        .done(function(data) {
            spokenMessage = data['spokenPassage'];
            if(data['relevantPassages'].length > 1) {
              botMessage = spokenMessage + "<br/><b>For more details:</b><br/>" + data['relevantPassages'].join('<br/><br/>')
              + "<br/><br/>" + data['url'] + "<br/><br/><i><u>If you are still not happy, please re-frame your question to be more specific.</u></i>";
              spokenMessage = spokenMessage + " For more details, please refer following articles and links";
            } else {
              botMessage = spokenMessage;
            }
            stopThinking();
            postCallback(true);
        }).fail(function() {
            botMessage = "I can't process your request right now.";
            stopThinking();
        });
        postCallback(false);
        startThinking();
}


function startThinking() {
    $("#chatbox").prop('disabled', true)
    $("#chatbox").attr("placeholder", "Thinking...");
}

function stopThinking() {
    $("#chatbox").prop('disabled', false)
    $("#chatbox").attr("placeholder", "");
}


function newEntry() {

    //if the message from the user isn't empty then run

    if (document.getElementById("chatbox").value !== "") {
        //pulls the value from the chatbox ands sets it to lastUserMessage
        lastUserMessage = document.getElementById("chatbox").value;
        //sets the chat box to be clear
        document.getElementById("chatbox").value = "";
        //adds the value of the chatbox to the array messages
        messages.push("<b>Me:</b> " + lastUserMessage);
        //Speech(lastUserMessage); //says what the user typed outloud

        //sets the variable botMessage in response to lastUserMessage

        chatbotResponse(postCallback);
    }

}

function postCallback(isResponse) {
    //add the chatbot's name and message to the array messages

    // says the message using the text to speech function written below
    if(isResponse === true) {
        messages.push("<b>" + botName + ":</b> " + botMessage);
        Speech(spokenMessage);
    }

    for (var i = 1; i < 8; i++) {
        if (messages[messages.length - i])
            document.getElementById("chatlog" + i).innerHTML = messages[messages.length - i];
    }

}

function displayChatLog() {
//outputs the last few array elements of messages to html
    for (var i = 1; i < 8; i++) {
        if (window.CP.shouldStopExecution(1)) {
            break;
        }
        if (window.CP.shouldStopExecution(1)) {
            break;
        }

        if (messages[messages.length - i])
            document.getElementById("chatlog" + i).innerHTML = messages[messages.length - i];

    }
    window.CP.exitedLoop(1);
    window.CP.exitedLoop(1);
}

function talk_check_clicked() {
    if ($('#talk_check').attr("checked")) {
        $('#talk_text').text("Stop talking");
        alert("on");
    } else {
        $('#talk_text').text("Start talking");
        alert("off");
    }
}

function Speech(say) {

    if ('speechSynthesis' in window && talking) {

        var utterance = new SpeechSynthesisUtterance(say);

        //msg.voice = voices[10]; // Note: some voices don't support altering params

        //msg.voiceURI = 'native';

        //utterance.volume = 1; // 0 to 1

        //utterance.rate = 0.1; // 0.1 to 10

        //utterance.pitch = 1; //0 to 2

        //utterance.text = 'Hello World';

        //utterance.lang = 'en-US';

        speechSynthesis.speak(utterance);

    }

}

//runs the keypress() function when a key is pressed

document.onkeypress = keyPress;

//if the key pressed is 'enter' runs the function newEntry()

function keyPress(e) {

    var x = e || window.event;

    var key = (x.keyCode || x.which);

    if (key == 13 || key == 3) {
        newEntry();
    }

    if (key == 38) {
        console.log('hi')
        //document.getElementById("chatbox").value = lastUserMessage;
    }

}

function placeHolder() {
    document.getElementById("chatbox").placeholder = "";
}

//# sourceURL=pen.js